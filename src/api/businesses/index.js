/* api/items/index.js */
import 'whatwg-fetch';
import uuidV1 from 'uuid';
// import receiveStatus from '../receiveStatus';

export default class businessesApi {
  static createBusinessPromise(business) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          id: uuidV1(),
          name: business.name,
          description: business.description,
          address: business.address,
          phone: business.phone,
          website: business.website,
          foodType: business.foodType,
          loginDate: new Date(),
        });
      }, 100);
    });
    // return fetch('/items', {
    //   method: 'POST',
    //   credentials: 'same-origin',
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify(item),
    // })
    //   .then(res => receiveStatus(res))
    //     .then(res => res.json())
    //   .catch(err => receiveStatus(err));
  }

  static deleteBusinessPromise(business) {
    return new Promise((resolve) => {
      console.log(business);
      setTimeout(() => {
        resolve();
      }, 100);
    });
    // return fetch('/items', {
    //   method: 'POST',
    //   credentials: 'same-origin',
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify(item),
    // })
    //   .then(res => receiveStatus(res))
    //     .then(res => res.json())
    //   .catch(err => receiveStatus(err));
  }

  static updateBusinessPromise(business) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          name: business.name,
          description: business.description,
          address: business.address,
          phone: business.phone,
          website: business.website,
          foodType: business.foodType,
        });
      }, 100);
    });
    // return fetch('/items', {
    //   method: 'POST',
    //   credentials: 'same-origin',
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify(item),
    // })
    //   .then(res => receiveStatus(res))
    //     .then(res => res.json())
    //   .catch(err => receiveStatus(err));
  }
}
