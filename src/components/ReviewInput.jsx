import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  item: PropTypes.shape({}),
  restaurant: PropTypes.shape({}),
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
};

const defaultProps = {
  item: {
    review: '',
    rating: 0,
  },
  type: '',
  restaurant: {},
};

const ReviewInput = ({ item, restaurant, onClick, onChange, type }) => (
  <div className="addReview">
    <h2 className="subtitle">{`${type}`} a Review</h2>
    <label htmlFor="review">What do you love about {restaurant.name}?<br /> What can be improved?</label>
    <p>
      <textarea
        name="review"
        placeholder="Enter your review..."
        className="textarea"
        onChange={onChange}
        value={item ? item.review : ''}
      />
    </p>
    <label htmlFor="rating">How do you rate {restaurant.name}?</label>
    <div className="control">
      <label htmlFor="rating" className="radio">
        <input
          type="radio"
          name="rating"
          value="1"
          onChange={onChange}
        /><br />1
      </label>
      <label htmlFor="rating" className="radio">
        <input
          type="radio"
          name="rating"
          value="2"
          onChange={onChange}
        /><br />2
      </label>
      <label htmlFor="rating" className="radio">
        <input
          type="radio"
          name="rating"
          value="3"
          onChange={onChange}
        /><br />3
      </label>
      <label htmlFor="rating" className="radio">
        <input
          type="radio"
          name="rating"
          value="4"
          onChange={onChange}
        /><br />4
      </label>
      <label htmlFor="rating" className="radio">
        <input
          type="radio"
          name="rating"
          value="5"
          onChange={onChange}
        />
        <br />5
      </label>
    </div>
    <button
      className="button is-primary"
      onClick={onClick}
    >{`${type}`} Review</button>
  </div>
);

ReviewInput.propTypes = propTypes;
ReviewInput.defaultProps = defaultProps;

export default ReviewInput;
