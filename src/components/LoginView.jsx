import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { USER_CREATE_REQ } from '../actions';

const propTypes = {
  users: PropTypes.array,
  dispatch: PropTypes.func.isRequired,
};

const defaultProps = {
  users: [],
};


class LoginView extends Component {
  state = {
    // get the users from connect and redux
    currentUser: this.props.users[0],
  }

  onFieldChange = (event) => {
    // each field that uses this onFieldChange
    // will append the changed field to state
    this.setState({
      currentUser: {
        ...this.state.currentUser,
        [event.target.name]: event.target.value,
      },
    });
  };

  processLogin = () => {
    // console.log('i need to submit to redux');
    const { currentUser } = this.state;
    this.props.dispatch({
      type: USER_CREATE_REQ,
      user: currentUser,
    });
    console.log(currentUser);
    this.setState({ currentItem: { review: '', rating: 0, reviewDate: '' } });
  };

  render() {
    return (
      <div>
        <h1 className="title">Login</h1>
        <div className="field">
          <label className="label" htmlFor="user">Username</label>
          <input
            type="text"
            name="username"
            className="input"
            onChange={this.onFieldChange}
          />
        </div>
        <div className="field">
          <label className="label" htmlFor="pass">Password</label>
          <input
            type="password"
            name="password"
            className="input"
            onChange={this.onFieldChange}
          />
        </div>
        <div className="field">
          <button
            className="button is-primary"
            onClick={this.processLogin}
          >Login</button>
          <div>
            <pre>
              {JSON.stringify(this.state, null, 2) }
            </pre>
          </div>
        </div>
      </div>
    );
  }
}

// set the propTypes
LoginView.propTypes = propTypes;
LoginView.defaultProps = defaultProps;

function mapStateToProps(state) {
  // show everything in state
  // console.log(state);
  return {
    users: state.users,
  };
}

export default connect(mapStateToProps)(LoginView);
