import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import BusinessInput from './BusinessInput';
import { USER_CREATE_REQ } from '../actions';

const formFields = [
  { name: 'name', label: 'Business Name', type: 'text' },
  { name: 'description', label: 'Short Description', type: 'text' },
  { name: 'address', label: 'Full Address', type: 'text' },
  { name: 'phone', label: 'Phone Number', type: 'text' },
  { name: 'website', label: 'Website', type: 'text' },
  { name: 'foodType', label: 'What type of food?', type: 'text' },
];

const propTypes = {
  businesses: PropTypes.array,
  dispatch: PropTypes.func.isRequired,
};

const defaultProps = {
  businesses: [],
};

class BusinessView extends Component {
  state = {
    // get the users from connect and redux
    currentBusiness: this.props.businesses[0],
  }

  onFieldChange = (event) => {
    // each field that uses this onFieldChange
    // will append the changed field to state
    this.setState({
      currentBusiness: {
        ...this.state.currentBusiness,
        [event.target.name]: event.target.value,
      },
    });
  };

  processLogin = () => {
    const { currentBusiness } = this.state;
    this.props.dispatch({
      type: USER_CREATE_REQ,
      user: currentBusiness,
    });
    // console.log(currentBusiness);
    this.setState({ currentItem: { review: '', rating: 0, reviewDate: '' } });
  };

  render() {
    return (
      <div>
        <h1 className="title">Add your Business</h1>
        { formFields.map((field) => {
          return (
            <BusinessInput
              attributes={field}
              item={this.state}
              onChange={this.onFieldChange}
            />
          );
        })}
        <div className="field">
          <button
            className="button is-primary"
            onClick={this.processLogin}
          >Add Business</button>
          <div>
            <pre>
              {JSON.stringify(this.state, null, 2) }
            </pre>
          </div>
        </div>
      </div>
    );
  }
}

// set the propTypes
BusinessView.propTypes = propTypes;
BusinessView.defaultProps = defaultProps;

function mapStateToProps(state) {
  // show everything in state
  // console.log(state);
  return {
    businesses: state.users,
  };
}

export default connect(mapStateToProps)(BusinessView);
