import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  item: PropTypes.shape({}),
  attributes: PropTypes.shape({}),
  onChange: PropTypes.func.isRequired,
};

const defaultProps = {
  item: {},
  attributes: {},
};

const BusinessInput = ({ item, attributes, onChange }) => (
  <div className={`field item-${attributes.name}`}>
    <label className="label" htmlFor={attributes.name}>{attributes.label}</label>
    <input
      type={attributes.type}
      name={attributes.name}
      placeholder={`Enter your ${attributes.name}...`}
      className="input"
      onChange={onChange}
      value={item ? item[attributes.name] : ''}
    />
  </div>
);

BusinessInput.propTypes = propTypes;
BusinessInput.defaultProps = defaultProps;

export default BusinessInput;
