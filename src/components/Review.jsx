import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  review: PropTypes.shape({}),
  onClick: PropTypes.func.isRequired,
};

const defaultProps = {
  review: {},
};

const getStars = (num) => {
  const stars = parseInt(num, 10);
  const arr = [];
  for (let i = 0; i < stars; i += 1) {
    arr.push({ starId: i, star: i });
  }
  return arr;
};

const Review = ({ review, onClick }) => {
  const stars = getStars(review.item.rating);
  //console.log(review);
  return (
    <div className="card review" onClick={onClick}>
      <div className="card-content">
        <div className="content">
          <p>{review.item.review}</p>
          <p className="subtitle">
            {stars.map((star) => {
              const { starId } = star;
              return <i key={starId} className="fa fa-star" />;
            })}
            <small> {review.item.reviewDate.toString()}</small>
          </p>
        </div>
      </div>
    </div>
  );
};

Review.propTypes = propTypes;
Review.defaultProps = defaultProps;

export default Review;
