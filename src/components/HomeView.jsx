import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ITEM_CREATE_REQ, REVIEW_UPDATE_REQ } from '../actions';

import ReviewInput from './ReviewInput';
import Review from './Review';

const restaurant = {
  id: 2001,
  name: 'Jalapenos',
  description: 'Mexican restaurant',
  address: '795 E 700 S, St George, UT 84770',
  phone: '435-656-9146',
  website: null,
  foodType: 'Mexican',
  dateCreated: new Date(),
};

const defaultProps = {
  items: [],
};

const propTypes = {
  items: PropTypes.array,
  dispatch: PropTypes.func.isRequired,
};


class HomeView extends Component {
  state = {
    currentItem: this.props.items[0],
  }

  onReviewClick = () => {
    const { currentItem } = this.state;
    this.props.dispatch({
      type: ITEM_CREATE_REQ,
      item: currentItem,
    });
    // console.log(currentItem);
    this.setState({ currentItem: { review: '', rating: 0, reviewDate: '' } });
  }

  onFieldChange = (event) => {
    // each field that uses this onFieldChange
    // will append the changed field to state
    this.setState({
      currentItem: {
        ...this.state.currentItem,
        [event.target.name]: event.target.value,
      },
    });
  };


  onReviewCreate = () => {
    const { currentItem } = this.state;
    this.props.dispatch({
      type: ITEM_CREATE_REQ,
      item: currentItem,
    });
    // console.log(currentItem);
    this.setState({ currentItem: { review: '', rating: 0, reviewDate: '' } });
  }

  onReviewUpdate = (review) => {
    this.props.dispatch({
      type: REVIEW_UPDATE_REQ,
      item: review,
    });
    this.setState({ review: null });
  }

  onItemDelete = (editItem, callback) => {
    this.props.dispatch({
      type: ITEM_DELETE_REQ,
      item: editItem,
    });
    if (callback) callback();
  }

  renderDeleteModal = (editItem) => {
    const {
      showDeleteModal,
      editItem,
    } = this.state;
    if(showDeleteModal) {
      return (
        <div>
          <h3>{`${editItem.name}`} will be deleted forever.</h3>
          <button onClick={() => {
            this.onItemDelete(editItem, () => {
              this.setState({
                showDeleteModal: false,
                editItem: null,
              })
            })
          }}
          >Delete</button>
        </div>
      );
    }
  };

  render() {
    const { items } = this.props;
    const { currentItem } = this.state;

    return (
      <div>
        <div className="columns">
          <div className="column is-4">
            <h1 className="title">
              Review { restaurant.name }
            </h1>
            <h2 className="subtitle">
              { restaurant.description }
            </h2>
            <p>
              <a
                href={`https://www.google.com/maps?q=${restaurant.address}`}
              >
                {restaurant.address}
              </a><br />
              { restaurant.phone }<br />
              <br />
            </p>
            <ReviewInput
              item={currentItem}
              type="Add"
              onChange={this.onFieldChange}
              onClick={this.onReviewCreate}
              restaurant={restaurant}
            />

          </div>
          <div className="column">
            <h2 className="title is-3">Current Reviews</h2>
            { items.map((item) => {
              return (
                <Review
                  review={{ item }}
                  key={item.id}
                  onChange={this.onReviewUpdate}
                  onClick={this.onReviewChange}
                />
              );
              // const { id, review, rating, reviewDate } = item;
            })}
          </div>
        </div>
      </div>
    );
  }
}

HomeView.propTypes = propTypes;
HomeView.defaultProps = defaultProps;

function mapStateToProps(state) {
  return {
    items: state.items,
  };
}

export default connect(mapStateToProps)(HomeView);
