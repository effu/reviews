// import all the actions from your actions file
import * as actions from '../actions';

export default function users(state = [], action) {
  switch (action.type) {
    case actions.USER_CREATE_RES:
      // spread in current state and add the action.user
      return [...state, action.user];
    default:
      return state;
  }
}
