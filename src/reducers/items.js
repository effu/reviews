import * as actions from '../actions';


const findIndex = (array, id) => {
  let itemIndex = -1;
  array.forEach((item, index) => {
    if (item.id === id) {
      itemIndex = index;
    }
  });
  return itemIndex;
};

export default function items(state = [], action) {
  let index;
  let moz;
  switch (action.type) {
    case actions.ITEM_CREATE_RES:
      return [...state, action.item];
    case actions.ITEM_DELETE_RES:
      index = findIndex(state, action.item.id);
      if (index === -1) {
        return state;
      }
      // takes state and slice of it from index 0 to the actual index
      moz = [
        ...state.slice(0, index),
        ...state.slice(index + 1),
      ];
      return moz;

    case actions.ITEM_UPDATE_RES:
      index = findIndex(state, action.item.id);
      if (index === -1) {
        return state;
      }
      // takes state and slice of it from index 0 to the actual index
      moz = [
        ...state.slice(0, index),
        action.item,
        ...state.slice(index + 1),
      ];
      return moz;
    default:
      return state;
  }
}
